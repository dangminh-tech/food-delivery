import { View, Text } from 'react-native'
import React, { useState, useEffect, useRef } from 'react'
import DropDownPicker from 'react-native-dropdown-picker';
import { countries } from './Common';
import { TextInput } from 'react-native-paper';

// const listCountries = countries.map(item => ({ label: item.name, value: item.code }))

export default function MyList() {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState(countries.map(item => ({ label: item.name, value: item.code })));
    const [search, setSearch] = useState('')
    const [listSearch, setListSearch] = useState([])


    useEffect(() => {
        if (search) {
            // const listSearch = listCountries.filter(item => item.label.toLowerCase().includes(search.toLowerCase()));
            const listSearch = countries.reduce((array, item) => {
                if (item.name.toLowerCase().includes(search.toLowerCase())) {
                    array.push({
                        label: item.name,
                        value: item.code
                    })
                }
                return array;
            }, [])
            setItems(listSearch);
        } else {
            setItems(countries.map(item => ({ label: item.name, value: item.code })))
        }
    }, [search])

    const searchRef = useRef();

    useEffect(() => {
        if (open) {
            searchRef?.current?.focus();
        } else {
            searchRef?.current?.blur();
        }
    }, [open])

    return (
        <View style={{ paddingHorizontal: 15 }}>
            {open && <TextInput
                onChangeText={text => {
                    setSearch(text);
                }}
                value={search}
                ref={searchRef}
                style={{ marginVertical: 20, backgroundColor: '#fff', borderWidth: 1 }} placeholder={'Search...'} />}
            <DropDownPicker
                open={open}
                value={value}
                items={items}
                setOpen={setOpen}
                setValue={setValue}
                setItems={setItems}
            />


        </View>
    );
}