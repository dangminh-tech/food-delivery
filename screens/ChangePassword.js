import { View, Text, Alert, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'
import Header from '../components/Header'
import InputPrimary from '../components/InputPrimary'
import ButtonPrimary from '../components/ButtonPrimary'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function ChangePassword({ navigation }) {

    const [password, setPassword] = useState('');
    const [newPass, setNewPass] = useState('');
    const [rePass, setRePass] = useState('');
    const [user, setUser] = useState('');
    const [users, setUsers] = useState('');
    useEffect(() => {
        AsyncStorage.getItem('users').then(users => setUsers(JSON.parse(users)))
        AsyncStorage.getItem('user').then(user => setUser(JSON.parse(user)))

    }, [])


    const save = () => {
        console.log(users);
        if (password == user.password && newPass == rePass) {
            const index = users.findIndex(item => item.username == user.username && item.password == user.password)
            users.splice(index, 1, {
                ...user,
                username: user.username,
                password: newPass
            })
            AsyncStorage.setItem('users', JSON.stringify([...users]));
            navigation.navigate('Profile');
            Alert.alert('Success', 'Change Password Success', [{ title: 'ok' }])
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <Header title={'Change Password'} onPress={() => navigation.navigate('Profile')} />
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
                <ScrollView style={{ paddingHorizontal: 30, paddingTop: 30 }}>
                    <Text style={{ marginBottom: 20 }}>Enter Old Password</Text>
                    <InputPrimary
                        placeholder={'Password'} style={{ marginBottom: 30 }}
                        secure={true}
                        value={password}
                        setValue={setPassword}
                    />
                    <Text style={{ marginBottom: 20 }}>Create New Password</Text>
                    <InputPrimary
                        placeholder={'Enter New Password'}
                        secure={true} style={{ marginBottom: 20 }}
                        value={newPass}
                        setValue={setNewPass}
                    />
                    <InputPrimary
                        placeholder={'Re-enter New Password'}
                        secure={true}
                        value={rePass}
                        setValue={setRePass}
                    />
                    <ButtonPrimary
                        text={'Save'}
                        btnStyle={{ marginTop: 200 }}
                        onPress={save}
                    />
                </ScrollView>

            </View>
        </View>
    )
}