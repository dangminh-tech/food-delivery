import { View, Text } from 'react-native'
import React from 'react'
import Header from '../components/Header'

export default function MenuItem({ route, navigation }) {
    const { item } = route.params;
    return (
        <View>
            <Header title={item.title} onPress={() => navigation.navigate('Home')} />
            <Text>{item.title}</Text>
        </View>
    )
}