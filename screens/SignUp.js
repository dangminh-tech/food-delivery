import { View, Text, TouchableOpacity, Dimensions, Alert, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'

import ButtonPrimary from '../components/ButtonPrimary'
import InputPrimary from '../components/InputPrimary'
import Footer from '../components/Footer'
import Back from '../assets/svg/chevron-left.svg'
import Header from '../components/Header'
import AsyncStorage from '@react-native-async-storage/async-storage'

const height = Dimensions.get('window').height
export default function SignUp({ navigation }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [rePassword, setRePassword] = useState('');

    const [users, setUsers] = useState([]);
    useEffect(() => {
        AsyncStorage.getItem('users').then(users => setUsers(JSON.parse(users)))
    }, [])

    const signUp = () => {
        if (
            username.length > 0 &&
            password.length > 0 &&
            rePassword.length > 0 &&
            password == rePassword
        ) {
            AsyncStorage.setItem('users', JSON.stringify([...users, {
                username,
                password,
                firstName: '',
                lastName: '',
                birthdate: '',
                email: '',
                nation: '',
                credits: [

                ]
            }]))
            navigation.navigate('SignIn')
        } else {
            Alert.alert('Failed')
        }
    }

    return (
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
            <Header onPress={() => navigation.navigate('SignIn/Up')} />
            <ScrollView>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                    <Text style={{ alignSelf: 'flex-start', marginLeft: 30, color: '#000', fontSize: 24, fontWeight: '700' }}>Sign Up</Text>
                    <InputPrimary
                        placeholder={'Enter Username'}
                        style={{ marginBottom: 20, marginTop: 30 }}
                        value={username}
                        setValue={setUsername}
                    />
                    <InputPrimary
                        placeholder={'Enter Password'}
                        style={{ marginBottom: 20 }} secure={true}
                        value={password}
                        setValue={setPassword}
                    />
                    <InputPrimary
                        placeholder={'Re-enter Password'}
                        style={{ marginBottom: 30 }} secure={true}
                        value={rePassword}
                        setValue={setRePassword}
                    />
                    <ButtonPrimary text={'Sign Up'} onPress={signUp} />
                    <Text style={{ alignSelf: 'flex-end', marginRight: 30, marginTop: 20 }}>Forgot Password?</Text>
                </View>
                <View style={{ marginTop: 40 }}>
                    <Footer />
                </View>
            </ScrollView>
        </View >
    )
}