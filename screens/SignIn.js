import { View, Text, TouchableOpacity, Dimensions, Alert, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'
import ButtonPrimary from '../components/ButtonPrimary'
import InputPrimary from '../components/InputPrimary'
import Footer from '../components/Footer'
import Back from '../assets/svg/chevron-left.svg'
import Header from '../components/Header'
import AsyncStorage from '@react-native-async-storage/async-storage'

const height = Dimensions.get('window').height



export default function SignIn({ navigation }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [users, setUsers] = useState([]);
    useEffect(() => {
        AsyncStorage.getItem('users').then(users => setUsers(JSON.parse(users)))
    }, [])

    const signIn = () => {
        if (username.length > 0 && password.length > 0) {
            const user = users.find(user => {
                return user.username === username && user.password === password
            })
            if (user) {
                AsyncStorage.setItem('user', JSON.stringify(user))
                changScreen();
            } else {
                Alert.alert('Failed!');
            }
        }
    }
    const changScreen = () => {
        navigation.navigate('Main')
    }

    return (
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
            <Header onPress={() => navigation.navigate('SignIn/Up')} />
            <ScrollView>
                <View style={{ flex: 1, alignItems: 'center', marginTop: 70 }}>
                    <Text style={{ alignSelf: 'flex-start', marginLeft: 30, color: '#000', fontSize: 24, fontWeight: '700' }}>Sign In</Text>
                    <InputPrimary
                        placeholder={'Username'}
                        style={{
                            marginBottom: 20,
                            marginTop: 30
                        }}
                        value={username}
                        setValue={setUsername}
                    />
                    <InputPrimary
                        placeholder={'Password'}
                        style={{ marginBottom: 30 }}
                        secure={true}
                        value={password}
                        setValue={setPassword}
                    />
                    <ButtonPrimary
                        text={'Sign In'}
                        onPress={signIn}
                    />
                    <Text style={{ alignSelf: 'flex-end', marginRight: 30, marginTop: 20 }}>Forgot Password?</Text>
                </View>
                <View style={{ marginTop: 90 }}>
                    <Footer />

                </View>
            </ScrollView>
            {/* <Footer /> */}

        </View >
    )
}