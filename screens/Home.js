import { View, Text, TouchableOpacity, FlatList, Image, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import Back from '../assets/svg/chevron-left.svg'
import Loupe from '../assets/svg/loupe.svg'
import Pin from '../assets/svg/pin.svg'
import { useWindowDimensions } from 'react-native';
import Restaurant from '../components/Restaurant'



const drink = require('../assets/img/coffee-cup.png')
const food = require('../assets/img/burger.png')
const cake = require('../assets/img/piece-of-cake.png')
const snack = require('../assets/img/potato-chips.png')

import InputPrimary from '../components/InputPrimary'
import { listRestaurants, listMenu, listDrink, listCake, listSnack } from './Common'

const type = [
    {
        title: 'Drink',
        image: drink
    },
    {
        title: 'Food',
        image: food
    },
    {
        title: 'Cake',
        image: cake
    },
    {
        title: 'Snack',
        image: snack
    },
]



const TypeItem = ({ item, index, setFoodMenu, indexType, setIndexType }) => {
    return (
        <View style={[{ alignItems: 'center', marginRight: 40 }]}>
            <TouchableOpacity
                style={[styles.typeItem, { backgroundColor: indexType == index ? '#D35400' : '#ECF0F1' }]}
                onPress={() => {
                    setIndexType(index);
                    if (index == 0) {
                        setFoodMenu(listDrink)
                    } else if (index == 1) {
                        setFoodMenu(listMenu)
                    } else if (index == 2) {
                        setFoodMenu(listCake)
                    } else if (index == 3) {
                        setFoodMenu(listSnack)
                    }
                }}
            >
                <Image source={item.image} />
            </TouchableOpacity>
            <Text style={{ marginTop: 5 }}>{item.title}</Text>
        </View>
    )
}

const FoodItem = ({ item, index, navigation }) => {
    return (
        <View>
            <TouchableOpacity onPress={() => navigation.navigate('MenuItem', { item: item[0] })} style={[{ backgroundColor: index % 2 == 0 ? '#c3e1f5' : '#e1ceea' }, styles.foodItem]}>
                <Text style={{ position: 'absolute', color: '#fff', fontSize: 14, fontWeight: '700', left: 10, top: 10 }}>{item[0].title}</Text>
                <Image source={item[0].image} style={{ position: 'absolute', right: 0, borderBottomRightRadius: 20 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('MenuItem', { item: item[1] })} style={[{ backgroundColor: index % 2 == 0 ? '#e1ceea' : '#c3e1f5' }, styles.foodItem]}>
                <Text style={{ position: 'absolute', color: '#fff', fontSize: 14, fontWeight: '700', left: 10, top: 10 }}>{item[1].title}</Text>
                <Image source={item[1].image} style={{ position: 'absolute', right: 0 }} />
            </TouchableOpacity>
        </View>

    )
}



export default function Home({ navigation }) {
    const [foodMenu, setFoodMenu] = useState(listMenu);
    const [indexType, setIndexType] = useState(1);
    const { height, width } = useWindowDimensions();
    return (
        <FlatList
            data={[1]}
            renderItem={() =>
                <View style={{ paddingLeft: 25, backgroundColor: '#fff' }}>
                    {/* <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
                        <Back />
                    </TouchableOpacity> */}
                    <View style={{ flexDirection: 'row', alignItems: 'center', position: 'relative', marginTop: 10 }}>
                        <Loupe style={{ position: 'absolute', left: 20, zIndex: 999 }} />
                        <InputPrimary
                            placeholder={'Search'}
                            style={{ paddingLeft: 55 }}
                            onFocus={() => navigation.navigate('SearchRestaurants')}
                            showKey={false}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 13, marginLeft: 15 }}>
                        <Pin />
                        <Text style={{ marginLeft: 5 }}>9 West 46 Th Street, New York City</Text>
                    </View>
                    <FlatList
                        data={type}
                        renderItem={({ item, index }) => {
                            return <TypeItem item={item} index={index} setFoodMenu={setFoodMenu} setIndexType={setIndexType} indexType={indexType} />
                        }}
                        horizontal
                        style={{ marginTop: 20, paddingBottom: 5 }}
                    />

                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginRight: 30, marginTop: 10 }}>
                        <Text style={styles.textMenu}>Food Menu</Text>
                        <Text>View all</Text>
                    </TouchableOpacity>

                    <FlatList
                        data={foodMenu}
                        renderItem={({ item, index }) => {
                            return <FoodItem item={item} index={index} navigation={navigation} />
                        }}
                        ListEmptyComponent={() => {
                            return (
                                <View style={{ height: 300, width: width - 60, backgroundColor: '#ccc' }}>
                                    <Text>Empty</Text>
                                </View>
                            )
                        }}
                        horizontal
                        style={{ marginTop: 10 }}
                    />

                    <TouchableOpacity onPress={() => navigation.navigate('SearchRestaurants')} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginRight: 30 }}>
                        <Text style={styles.textMenu}>Near Me</Text>
                        <Text>View all</Text>
                    </TouchableOpacity>

                    <FlatList
                        data={listRestaurants.slice(0, 3)}
                        renderItem={({ item, index }) => {
                            return <Restaurant item={item} index={index} />
                        }}
                        style={{ marginTop: 10 }}
                    />

                </View>
            }

        />
    )
}

const styles = StyleSheet.create({
    typeItem: {
        height: 70, width: 70,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    textMenu: {
        fontSize: 18,
        fontWeight: '700'
    },
    foodItem: {
        marginRight: 20, marginBottom: 20,
        borderRadius: 20,
        height: 130,
        width: 130
        // position: 'relative'
    }


})