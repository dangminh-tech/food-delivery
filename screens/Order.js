import { View, Text, Image, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import React, { useState, useEffect } from 'react'
import Header from '../components/Header'
import Pin2 from '../assets/svg/pin2.svg'
import Clock from '../assets/svg/clock.svg'
import Like from '../assets/svg/like.svg'
import Dislike from '../assets/svg/dislike.svg'
import ButtonPrimary from '../components/ButtonPrimary'


const OrderItem = () => {
    const [likeNumber, setLikeNumber] = useState(1000);
    const [dislikeNumber, setDislikeNumber] = useState(94);

    const [like, setLike] = useState(false);
    const [dislike, setDislike] = useState(false);

    useEffect(() => {
        if (like && typeof like === 'boolean') {
            setLikeNumber(pre => pre + 1);
        } else if (!like && typeof like === 'boolean') {
            setLikeNumber(pre => pre - 1);
        }
    }, [like])

    useEffect(() => {
        if (dislike && typeof dislike === 'boolean') {
            setDislikeNumber(pre => pre + 1);
        } else if (!dislike && typeof dislike === 'boolean') {
            setDislikeNumber(pre => pre - 1);
        }
    }, [dislike])

    return (
        <View style={{ flexDirection: 'row', marginBottom: 10, paddingHorizontal: 30, marginTop: 10 }}>
            <Image source={require('../assets/img/restaurant.png')} style={{ width: 80, height: 80 }} />
            <View style={{ marginLeft: 20 }}>
                <Text style={{ fontSize: 15, fontWeight: '700' }}>Dogmie jagong tutung</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                    <Like />
                    <Text> {likeNumber} | </Text>
                    <Dislike />
                    <Text> {dislikeNumber} </Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                    <Text style={{ fontSize: 14, fontWeight: '700', color: '#2ECC71' }}>$99.99</Text>
                </View>

            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, paddingLeft: 15 }}>
                <TouchableOpacity
                    style={dislike ? styles.btnLike : {}}
                    onPress={() => {
                        setDislike(!dislike)
                        setLike(false)
                    }}
                >
                    <Dislike />
                </TouchableOpacity>
                <TouchableOpacity style={like ? styles.btnLike : {}}
                    onPress={() => {
                        setLike(!like)
                        setDislike(false)
                    }}
                >
                    <Like />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default function Order({ navigation }) {
    return (
        <View style={{ flex: 1, position: 'relative', justifyContent: 'space-between' }}>
            <Header title={'Orders'} onPress={() => {
                navigation.navigate('Home')
            }} />
            <FlatList
                data={[1, 2, 3, 4, 5, 6, 7, 8, 9]}
                renderItem={() => {
                    return <OrderItem />
                }}
                ListFooterComponent={() => {
                    return (
                        <View style={{ height: 70 }}></View>
                    )
                }}
            />

            <ButtonPrimary text={'Send'} btnStyle={{ marginHorizontal: 30, marginVertical: 15 }} />

        </View>
    )
}

const styles = StyleSheet.create({
    btnLike: {
        backgroundColor: '#D35400',
        borderRadius: 14,
        width: 28,
        height: 28,
        justifyContent: 'center',
        alignItems: 'center'
    }
})