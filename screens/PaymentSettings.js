import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../components/Header'
import Paypal from '../assets/svg/paypal.svg'
import Credit from '../assets/svg/credit.svg'
import Wallet from '../assets/svg/wallet.svg'
import Plus from '../assets/svg/plus.svg'
import ChevronRight from '../assets/svg/chevron-right.svg'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useIsFocused } from '@react-navigation/native'

export default function PaymentSettings({ navigation }) {
    const [user, setUser] = useState('')
    const isFocused = useIsFocused();
    useEffect(() => {
        isFocused && AsyncStorage.getItem('user').then((user) => setUser(JSON.parse(user)));
    }, [isFocused])

    const CreditItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate('CardDetail', item)} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 30, marginTop: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ECF0F1', borderRadius: 5 }}>
                        <Credit />
                    </View>
                    <Text style={{ lineHeight: 30 }}> Credit Card</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{item.cardNumber}</Text>
                    <ChevronRight />
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header onPress={() => navigation.navigate('Profile')} title={'Payment Settings'} />
            <TouchableOpacity onPress={() => navigation.navigate('CardDetail')} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 30, marginTop: 30 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ECF0F1', borderRadius: 5 }}>
                        <Paypal />
                    </View>
                    <Text style={{ lineHeight: 30 }}> Paypal</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text>Itoh@gmail.com</Text>
                    <ChevronRight />
                </View>
            </TouchableOpacity>
            <FlatList
                data={user.credits}
                renderItem={({ item, index }) => {
                    return <CreditItem item={item} index={index} />
                }}
                ListFooterComponent={() => {
                    return (
                        <TouchableOpacity onPress={() => navigation.navigate('AddCard')} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 30, marginTop: 20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ECF0F1', borderRadius: 5 }}>
                                    <Wallet />
                                </View>
                                <Text style={{ lineHeight: 30 }}> Add new payment method</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ECF0F1', borderRadius: 5 }}>
                                    <Plus />
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />

        </View>
    )
}