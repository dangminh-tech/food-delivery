import { View, Text, TouchableOpacity } from 'react-native'
import React, { useState, useEffect } from 'react'
import Header from '../components/Header'
import Paypal from '../assets/svg/paypal.svg'
import Credit from '../assets/svg/credit.svg'
import Wallet from '../assets/svg/wallet.svg'
import Plus from '../assets/svg/plus.svg'
import ChevronRight from '../assets/svg/chevron-right.svg'
import ButtonPrimary from '../components/ButtonPrimary'
import AsyncStorage from '@react-native-async-storage/async-storage'


export default function CardDetail({ route, navigation }) {
    const item = route.params;

    const [user, setUser] = useState('');
    const [users, setUsers] = useState('');

    useEffect(() => {
        AsyncStorage.getItem('user').then((user) => setUser(JSON.parse(user)));
        AsyncStorage.getItem('users').then((users) => setUsers(JSON.parse(users)));
    }, [])

    const deleteCard = () => {
        const index = user.credits.findIndex(card => card.cardNumber == item.cardNumber)
        user.credits.splice(index, 1);
        AsyncStorage.setItem('user', JSON.stringify({ ...user }))
        const index2 = users.findIndex(i => i.username == user.username && i.password == user.password)
        users.splice(index2, 1, { ...user })
        AsyncStorage.setItem('users', JSON.stringify(users))
        navigation.navigate('PaymentSettings');
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header title={'Paypal'} onPress={() => navigation.goBack()} />
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 30, paddingHorizontal: 30 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ECF0F1', borderRadius: 5 }}>
                            {
                                item ? <Credit /> : <Paypal />
                            }
                        </View>
                        <Text style={{ lineHeight: 30 }}>{item ? ' Credit Card' : ' Paypal'} </Text>
                    </View>
                    <View onPress={() => navigation.navigate('CardDetail')} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{item ? item.cardNumber : 'Itoh@gmail.com'}</Text>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 30 }}>
                    <ButtonPrimary text={'Mark as Default'} btnStyle={{ marginBottom: 20 }} />
                    <ButtonPrimary
                        text={'Remove'}
                        btnStyle={{ marginBottom: 30, backgroundColor: '#ECF0F1' }}
                        txtStyle={{ color: '#000' }}
                        onPress={deleteCard}
                    />
                </View>
            </View>

        </View>
    )
}