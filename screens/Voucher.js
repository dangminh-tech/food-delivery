import { View, Text, TouchableOpacity, FlatList, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import Header from '../components/Header'
import Pin2 from '../assets/svg/pin2.svg'
import Clock from '../assets/svg/clock.svg'
import Like from '../assets/svg/like.svg'
import Dislike from '../assets/svg/dislike.svg'
import VoucherImg from '../assets/svg/voucher.svg'
import ButtonPrimary from '../components/ButtonPrimary'
import Tick from '../assets/svg/tick.svg'

const VoucherItem = () => {
    const [check, setCheck] = useState(false)
    return (
        <View style={{ flexDirection: 'row', marginBottom: 10, paddingHorizontal: 30, marginTop: 10 }}>
            {/* <Image source={require('../assets/img/restaurant.png')} style={{ width: 80, height: 80 }} /> */}
            <View style={{ width: 80, height: 80, backgroundColor: '#ECF0F1', borderRadius: 30 }}>
                <VoucherImg />
            </View>
            <View style={{ marginLeft: 20 }}>
                <Text style={{ fontSize: 15, fontWeight: '700' }}>Sale off 30% for Pizza</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                    <Clock />
                    <Text> Apr 10 - Apr 30</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                    <Text style={{ fontSize: 14, fontWeight: '700', color: '#E74C3C' }}>11 days left</Text>
                </View>

            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <TouchableOpacity style={check ? styles.checked : styles.unChecked} onPress={() => setCheck(!check)}>
                    <Tick />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default function Voucher({ navigation }) {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header title={'My Voucher'} onPress={() => navigation.navigate('Profile')} />
            <FlatList
                data={[1, 2, 3, 4, 5, 1, 1, 1, 1]}
                renderItem={({ item, index }) => {
                    return <VoucherItem />
                }}
                ListFooterComponent={() => {
                    return (
                        <View style={{ height: 70 }}></View>
                    )
                }}
            />
            <ButtonPrimary text={'Send'} btnStyle={{ marginLeft: 30, marginBottom: 30 }} />
        </View>
    )
}

const styles = StyleSheet.create({
    checked: {
        backgroundColor: '#D35400',
        justifyContent: 'center',
        width: 30,
        height: 30,
        alignItems: 'center', borderRadius: 15
    },
    unChecked: {
        backgroundColor: '#ECF0F1',
        justifyContent: 'center',
        width: 30,
        height: 30,
        alignItems: 'center', borderRadius: 15
    }
})