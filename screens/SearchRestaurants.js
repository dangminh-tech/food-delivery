import { View, Text, FlatList, } from 'react-native'
import React, { useState, useEffect } from 'react'
import Header from '../components/Header'
import { listRestaurants } from './Common'
import Restaurant from '../components/Restaurant'
import Loupe from '../assets/svg/loupe.svg'
import InputPrimary from '../components/InputPrimary'
import Back from '../assets/svg/chevron-left.svg'




export default function SearchRestaurants({ navigation }) {
    const [search, setSearch] = useState('');
    const [data, setData] = useState(listRestaurants)
    useEffect(() => {
        if (search) {
            const result = listRestaurants.reduce((arr, item) => {
                if (item.name.toLowerCase().includes(search.toLowerCase())
                    || item.address.toLowerCase().includes(search.toLowerCase())) {
                    arr.push(item)
                }
                return arr;
            }, [])
            setData(result)
        } else {
            setData(listRestaurants)
        }
    }, [search])

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Back style={{ marginHorizontal: 15 }} onPress={() => navigation.navigate('Home')} />
                <View style={{ flexDirection: 'row', alignItems: 'center', position: 'relative', marginTop: 10 }}>
                    <Loupe style={{ position: 'absolute', left: 20, zIndex: 999 }} />
                    <InputPrimary
                        placeholder={'Search'}
                        style={{ paddingLeft: 55 }}
                        value={search}
                        setValue={setSearch}
                    />
                </View>
            </View>
            <FlatList
                data={data}
                renderItem={({ item, index }) => {
                    return <Restaurant item={item} index={index} />
                }}
                style={{ marginTop: 20, paddingHorizontal: 30 }}
            />
        </View>
    )
}