import React, { useState, useRef } from 'react';
import { Text, View, Dimensions, Image } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MainNavigation from '../navigation/MainNavigation';
const chicken = require('../assets/img/chicken-leg.png')
const card = require('../assets/img/credit-card.png')
const medal = require('../assets/img/medal.png')
const shipped = require('../assets/img/shipped.png')
import SignInUp from './SignInUp';

export const SLIDER_WIDTH = Dimensions.get('window').width;
export const ITEM_WIDTH = Dimensions.get('window').width;

const data = [
    {
        id: 1,
        name: 'Delicious Food',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        url: chicken,
    },
    {
        id: 2,
        name: 'Fast Shipping',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        url: shipped,
    },
    {
        id: 3,
        name: 'Certificate Food',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        url: medal,
    },
    {
        id: 4,
        name: 'Payment Online',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        url: card,
    }
];


const renderItem = ({ item, index }) => {
    return (

        <View
            style={{
                // flex: 
                backgroundColor: '#D35400',
                height: '90%',
                justifyContent: 'center',
                alignItems: 'center'
                // backgroundColor: 'white',
            }}>
            <Image source={item.url} style={{ width: 200, height: 200 }} />
            <Text style={{ marginTop: 20, marginBottom: 15, fontSize: 24, fontWeight: '700', color: '#fff' }}>
                {item.name}
            </Text>
            <Text style={{ color: '#fff' }}>
                {item.description}
            </Text>
        </View>



    );
};

const Slide = ({ navigation }) => {
    const [index, setIndex] = useState(0);
    const isCarousel = useRef(null);
    return (
        <>
            {
                index == 4 ? (
                    <MainNavigation />
                ) : (<View style={{ backgroundColor: '#D35400' }}>
                    <Carousel
                        ref={isCarousel}
                        autoplay={true}
                        autoplayInterval={1000}
                        data={data}
                        renderItem={renderItem}
                        sliderWidth={SLIDER_WIDTH}
                        itemWidth={ITEM_WIDTH}
                        onSnapToItem={index => setIndex(index)}
                        onScrollEndDrag={event => {
                            console.log(event.nativeEvent)
                            const { contentSize, contentOffset } = event.nativeEvent;
                            console.log(contentOffset.x, contentSize.width * ((data.length - 1) / data.length))
                            // Detect when swipe right at the last item
                            if (index === data.length - 1 && contentOffset.x >= contentSize.width * ((data.length - 1) / data.length) - 1) {
                                navigation.navigate('SignIn/Up');
                            }
                        }}
                    />
                    <Pagination
                        dotsLength={data.length}
                        activeDotIndex={index}
                        carouselRef={isCarousel}
                        // containerStyle={{ backgroundColor: '#D35400' }}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            // marginHorizontal: 8,
                            backgroundColor: '#fff',
                            // position: 'absolute',
                            // top: 0,
                            // zIndex: 999
                        }}
                        tappableDots={true}
                        inactiveDotStyle={{
                            width: 18,
                            height: 18,
                            borderRadius: 9,
                            backgroundColor: 'rgba(255, 255, 255, 0.35)',
                            // position: 'absolute',
                            // top: 0,
                            // zIndex: 999
                            // Define styles for inactive dots here
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                </View>)
            }


        </>
    );
};

export default Slide;


