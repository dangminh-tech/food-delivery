import { View, Text, TextInput, TouchableOpacity, Image, StyleSheet, ScrollView, Alert } from 'react-native'

import React, { useState, useEffect, useRef, useReducer } from 'react'
import { Button } from 'react-native'
import DatePicker from 'react-native-date-picker'
import Header from '../components/Header'
import { countries } from './Common'
import DropDownPicker from 'react-native-dropdown-picker'
import AsyncStorage from '@react-native-async-storage/async-storage'
import ButtonPrimary from '../components/ButtonPrimary'

const listCountries = countries.map(item => ({ label: item.name, value: item.name }))

export default function MyProfile({ navigation }) {
    const [date, setDate] = useState(new Date())
    const [openDate, setOpenDate] = useState(false)

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(user?.nation);
    const [items, setItems] = useState(countries.map(item => ({ label: item.name, value: item.name })));
    const [search, setSearch] = useState('')
    const [listSearch, setListSearch] = useState([])


    const [user, setUser] = useState({})
    const [users, setUsers] = useState('');

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [birthdate, setBirthdate] = useState('');
    const [email, setEmail] = useState('');
    const [nation, setNation] = useState('');
    console.log(user);

    useEffect(() => {

    }, [])


    useEffect(() => {
        AsyncStorage.getItem('user').then(user => {
            setUser(JSON.parse(user))
            setFirstName(JSON.parse(user).firstName)
            setLastName(JSON.parse(user).lastName)
            console.log(JSON.parse(user).birthdate, '++++++++++++++++++')
            setBirthdate(new Date(JSON.parse(user).birthdate))
            setEmail(JSON.parse(user).email)
            setNation(JSON.parse(user).nation)
        })
        AsyncStorage.getItem('users').then(users => setUsers(JSON.parse(users)))

    }, [])

    console.log(birthdate, '-------------------');

    function isValidDate(d) {
        return d instanceof Date && !isNaN(d);
    }
    useEffect(() => {
        if (search) {
            const listSearch = listCountries.filter(item => item.label.toLowerCase().includes(search.toLowerCase()));
            setItems(listSearch);
        } else {
            setItems(listCountries)
        }
    }, [search])

    const boxRef = useRef();

    const [ignored, forceUpdate] = useReducer(x => x + 1, 0);

    useEffect(() => {
        // if (open) {
        //     searchRef?.current?.focus();
        // } else {
        //     searchRef?.current?.blur();
        // }
        boxRef.current.height = 300
        forceUpdate()
    }, [open])

    const save = () => {
        AsyncStorage.setItem('user', JSON.stringify({
            ...user,
            firstName,
            lastName,
            birthdate,
            email,
            nation
        }))
        const index = users.findIndex(item => item.username === user.username);
        users.splice(index, 1, {
            ...user,
            firstName,
            lastName,
            birthdate,
            email,
            nation
        })
        AsyncStorage.setItem('users', JSON.stringify([...users]))

        Alert.alert('success')
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header title={'My Profile'} onPress={() => navigation.navigate('Profile')} />
            <View style={{ justifyContent: 'space-between', flex: 1, paddingHorizontal: 30 }}>
                <ScrollView>
                    <View style={{ alignItems: 'center', marginBottom: 30 }}>
                        <Image source={require('../assets/img/avatar.png')} />
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.field}>Firstname: </Text>
                        <TextInput
                            style={styles.textField}
                            placeholder={user.firstName ? user.firstName : 'Type here'}
                            onChangeText={setFirstName}
                        >{firstName}</TextInput>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.field}>Lastname: </Text>
                        <TextInput
                            style={styles.textField}
                            placeholder={user.lastName ? user.lastName : 'Type here'}
                            onChangeText={setLastName}
                        >{lastName}</TextInput>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.field}>Birth: </Text>
                        <View style={{ flex: 2 }}>
                            <TouchableOpacity onPress={() => setOpenDate(true)}>
                                <Text style={styles.textField}>{isValidDate(birthdate) ? `${birthdate.getDate()}/${birthdate.getMonth()}/${birthdate.getFullYear()}` : 'mm/dd/yyyy'}</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <Text style={styles.textField}>dd/mm/yyyy</Text> */}
                    </View>
                    {/* <View style={styles.row}>
                    <Text style={styles.field}>Gender: </Text>
                    <TextInput style={styles.textField}>Male</TextInput>
                </View> */}
                    <View style={styles.row}>
                        <Text style={styles.field}>Email: </Text>
                        <TextInput
                            style={styles.textField}
                            placeholder={user.email ? user.email : 'Example@gmail.com'}
                            onChangeText={setEmail}
                        >{email}</TextInput>

                    </View>
                    <View style={[styles.row]} ref={boxRef}>
                        <Text style={styles.field}>Nationality: </Text>
                        {/* <TextInput style={styles.textField}>Hanoi Vietnam</TextInput> */}
                        <View style={{ flex: 2 }}>
                            {/* {open && <TextInput
                            onChangeText={text => {
                                setSearch(text);
                            }}
                            value={search}
                            ref={searchRef}
                            style={{ marginVertical: 20, backgroundColor: '#fff', borderWidth: 1 }} placeholder={'Search...'} />
                        } */}
                            <DropDownPicker
                                listMode='MODAL'
                                open={open}
                                value={value}
                                items={items}
                                searchable={true}
                                setOpen={setOpen}
                                setValue={(value) => {
                                    setValue(value);
                                    setNation(value);
                                }}
                                // style={{ borderColor: '#fff' }}
                                setItems={setItems}
                                placeholder={user.nation ? user.nation : 'Select a nationality'}
                                maxHeight={200}

                            />
                        </View>
                    </View>
                    <ButtonPrimary text={'Save'} btnStyle={{ marginTop: 50 }} onPress={save} />

                </ScrollView>

            </View>

            {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 }}>
                <Text>Name: </Text>
                <Text>minh</Text>
            </View> */}
            <DatePicker
                modal
                open={openDate}
                date={date}
                onConfirm={(date) => {
                    setOpenDate(false)
                    setDate(date)
                    setBirthdate(date)
                }}
                onCancel={() => {
                    setOpenDate(false)
                }}
                mode={'date'}
            // locale={'vi'}
            />


        </View>
    )
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginBottom: 15,
        borderBottomWidth: 1,
        paddingBottom: 10,
        borderColor: '#34495E'
    },

    field: {
        flex: 1
    },
    textField: {
        fontWeight: '500',
        color: '#000',
        flex: 2,
        fontSize: 14,
        padding: 0,
        height: 20
    }
})