import { View, Text, StyleSheet, Dimensions } from 'react-native'
import React from 'react'
import ButtonPrimary from '../components/ButtonPrimary'
import Burger from '../assets/svg/hamburger.svg'
import Footer from '../components/Footer'
const appWidth = Dimensions.get('window').width

export default function SignInUp({ navigation }) {
    return (
        <View style={styles.container}>
            <View style={{ alignItems: 'center', marginTop: 35 }}>
                <View style={{ width: 300, height: 300 }}>
                    <Burger />
                </View>
                <ButtonPrimary
                    text={'Sign In'}
                    btnStyle={{ marginTop: 60 }}
                    onPress={() => navigation.navigate('SignIn')}
                />
                <ButtonPrimary
                    text={'Sign Up'}
                    btnStyle={{ backgroundColor: '#ECF0F1', marginTop: 20 }}
                    txtStyle={{ color: '#000', }}
                    onPress={() => navigation.navigate('SignUp')}
                />
            </View>

            <Footer />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        // alignItems: 'center',
        // justifyContent: 'space-between'
    }
})