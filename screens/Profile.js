import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import Chevron from '../assets/svg/chevron-right.svg'
import ButtonPrimary from '../components/ButtonPrimary'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function Profile({ route, navigation }) {
    const [user, setUser] = useState({})
    useEffect(() => {
        AsyncStorage.getItem('user').then(user => setUser(JSON.parse(user)))
    }, [])

    const signOut = () => {
        AsyncStorage.removeItem('user');
        navigation.navigate('SignIn/Up');
    }

    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)

    return (
        <ScrollView style={{ backgroundColor: '#fff' }}>
            <View style={{ alignItems: 'center' }}>
                <Image source={require('../assets/img/avatar.png')} style={{ marginTop: 30 }} />
                <Text style={{ fontSize: 18, fontWeight: '700', marginTop: 20, marginBottom: 5 }}>{user.username}</Text>
                <Text>0919 583 889</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('MyProfile')} style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginBottom: 23, marginTop: 30 }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>My Profile</Text>
                <Chevron />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('ChangePassword')} style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginBottom: 23 }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>Change Password</Text>
                <Chevron />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('PaymentSettings')} style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginBottom: 23 }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>Payment Settings</Text>
                <Chevron />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Voucher')} style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginBottom: 23 }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>My Voucher</Text>
                <Chevron />
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginBottom: 23 }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>Notification</Text>
                <Chevron />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginBottom: 23 }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>About Us</Text>
                <Chevron />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginBottom: 23 }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>Contact Us</Text>
                <Chevron />
            </View>
            <ButtonPrimary
                text={'Sign Out'}
                btnStyle={{ marginLeft: 30, backgroundColor: '#ECF0F1', marginBottom: 10 }} txtStyle={{ color: '#000' }}
                onPress={signOut}
            />
        </ScrollView>
    )
}