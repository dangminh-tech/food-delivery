import { View, Text, Dimensions, TextInput, StyleSheet, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'
import Header from '../components/Header'
import Card from '../assets/svg/card.svg'
import CardMini from '../assets/svg/cardMini.svg'

import ButtonPrimary from '../components/ButtonPrimary';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DropDownPicker from 'react-native-dropdown-picker'
import { countries, bank } from './Common'

const width = Dimensions.get('window').width;


export default function AddCard({ navigation }) {
    // const [bank, setBank] = useState('');
    const [name, setName] = useState('');
    const [cardNumber, setCardNumber] = useState('');
    const [data, setData] = useState('');
    const [cvv, setCvv] = useState('');
    const [user, setUser] = useState('');
    const [users, setUsers] = useState('');

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState('');
    const [items, setItems] = useState(bank.map(item => ({ label: item.shortName, value: item.shortName })));

    useEffect(() => {
        AsyncStorage.getItem('user').then(user => setUser(JSON.parse(user)))
        AsyncStorage.getItem('users').then(users => setUsers(JSON.parse(users)))
    }, [])


    useEffect(() => {
        if (data.length == 2) {
            setData(pre => `${pre}/`)
        }
    }, [data])

    // useEffect(() => {
    //     if (cardNumber.length >= 4 && cardNumber.length % 4 == 0) {
    //         setCardNumber(pre => pre + ' ')
    //     }
    // }, [cardNumber])

    const add = () => {
        AsyncStorage.setItem('user', JSON.stringify({
            ...user,
            credits: [
                ...user.credits,
                {
                    bank,
                    name,
                    cardNumber,
                    data,
                    cvv
                }
            ]
        }))

        const index = users.findIndex(item => item.username === user.username && item.password == user.password);
        users.splice(index, 1, {
            ...user,
            credits: [
                ...user.credits,
                {
                    bank,
                    name,
                    cardNumber,
                    data,
                    cvv
                }
            ]
        })
        console.log(users)
        AsyncStorage.setItem('users', JSON.stringify([...users]))
        navigation.goBack();
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header title={'Add Credit Card'} onPress={() => navigation.goBack()} />
            <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center' }}>
                <View>
                    <View style={{ position: 'relative' }}>
                        <View style={{ backgroundColor: '#ECF0F1', marginTop: 30, borderRadius: 30 }}>
                            <Card />
                        </View>
                        <CardMini style={{ position: 'absolute', zIndex: 999, bottom: 20, right: 40 }} />
                        <Text style={{ position: 'absolute', zIndex: 999, left: 35, top: 50, color: '#fff', fontWeight: '700', fontSize: 18 }}>{name}</Text>
                        <Text style={{ position: 'absolute', zIndex: 999, left: 35, bottom: 80, fontSize: 10, color: '#fff' }}>{value ? `No ${value} Bank` : ''}</Text>
                        <Text style={{ position: 'absolute', zIndex: 999, right: 35, top: 50, fontSize: 10, color: '#fff' }}>{value.toUpperCase()}</Text>
                        <Text style={{ position: 'absolute', zIndex: 999, left: 35, bottom: 55, color: '#fff' }}>{cardNumber}</Text>
                        <Text style={{ position: 'absolute', zIndex: 999, left: 35, bottom: 30, color: '#fff', fontWeight: '700' }}>$12.999.999.99</Text>
                    </View>
                    <KeyboardAwareScrollView>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30, alignItems: 'center' }}>
                            <Text>Bank name</Text>
                            {/* <TextInput style={styles.field} value={bank} onChangeText={text => setBank(text)}></TextInput> */}
                            <View style={{ flex: 1 }}>
                                {/* {open && <TextInput
                            onChangeText={text => {
                                setSearch(text);
                            }}
                            value={search}
                            ref={searchRef}
                            style={{ marginVertical: 20, backgroundColor: '#fff', borderWidth: 1 }} placeholder={'Search...'} />
                        } */}
                                <DropDownPicker
                                    listMode='MODAL'
                                    open={open}
                                    value={value}
                                    items={items}
                                    searchable={true}
                                    setOpen={setOpen}
                                    setValue={(value) => {
                                        setValue(value);
                                    }}
                                    style={{ marginLeft: 35, width: 250 }}
                                    setItems={setItems}
                                    placeholder={'Select a bank'}
                                    maxHeight={200}

                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                            <Text>Your name</Text>
                            <TextInput style={styles.field} value={name} onChangeText={text => setName(text)}></TextInput>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                            <Text>Card Number</Text>
                            <TextInput style={styles.field} value={cardNumber} keyboardType='numeric' maxLength={20} onChangeText={text => setCardNumber(text)}></TextInput>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                            <Text>Data</Text>
                            <TextInput style={styles.field} value={data} onFocus={() => { setData('') }} maxLength={5} keyboardType='numeric' onChangeText={text => setData(text)}></TextInput>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                            <Text>CVV</Text>
                            <TextInput style={styles.field} value={cvv} keyboardType='numeric' onChangeText={text => setCvv(text)}></TextInput>
                        </View>
                        <View style={{ marginTop: 30 }}>
                            <ButtonPrimary text={'Add'} btnStyle={{ marginBottom: 30 }} onPress={add} />
                        </View>
                    </KeyboardAwareScrollView>
                </View>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    field: {
        fontSize: 14,
        fontWeight: '500',
        maxWidth: 250,
        height: 30,
        padding: 0,
        backgroundColor: '#ECF0F1',
        borderRadius: 5,
        flex: 1,
        textAlign: 'center'
    }
})