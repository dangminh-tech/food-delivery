import { View, Text } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import SignInUp from '../screens/SignInUp';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import Home from '../screens/Home';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Order from '../screens/Order';
import MyList from '../screens/MyList';
import Profile from '../screens/Profile';
import ChangePassword from '../screens/ChangePassword';
import PaymentSettings from '../screens/PaymentSettings';
import AddCard from '../screens/AddCard';
import CardDetail from '../screens/CardDetail';
import Voucher from '../screens/Voucher';
import DateTime from '../screens/MyProfile';
import MyProfile from '../screens/MyProfile';
import Slide from '../screens/Slide';
import MenuItem from '../screens/MenuItem';
import SearchRestaurants from '../screens/SearchRestaurants';



const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const Tabbar = () => {
    return <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => { //NOTES: HIển thị icon ở tabbar
                let iconName;

                if (route.name === 'Home') {
                    iconName = focused
                        ? 'home'
                        : 'home-outline';
                } else if (route.name === 'Order') {
                    iconName = focused ? 'book' : 'book-outline';
                } else if (route.name === 'MyList') {
                    iconName = focused ? 'bookmark' : 'bookmark-outline';
                } else if (route.name === 'Profile') {
                    iconName = focused ? 'person' : 'person-outline';
                }

                return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'tomato',
            tabBarInactiveTintColor: 'gray',
            headerShown: false
        })}
    >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Order" component={Order} />
        <Tab.Screen name="MyList" component={MyList} />
        <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
}

export default function MainNavigation() {
    return (
        <NavigationContainer >
            <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName='Slide'>
                <Stack.Screen name="Slide" component={Slide} />
                <Stack.Screen name="SearchRestaurants" component={SearchRestaurants} />
                <Stack.Screen name="MenuItem" component={MenuItem} />
                <Stack.Screen name="MyProfile" component={MyProfile} />
                <Stack.Screen name="SignIn/Up" component={SignInUp} />
                <Stack.Screen name="Main" component={Tabbar} />
                <Stack.Screen name="SignIn" component={SignIn} />
                <Stack.Screen name="SignUp" component={SignUp} />
                <Stack.Screen name="ChangePassword" component={ChangePassword} />
                <Stack.Screen name="PaymentSettings" component={PaymentSettings} />
                <Stack.Screen name="AddCard" component={AddCard} />
                <Stack.Screen name="CardDetail" component={CardDetail} />
                <Stack.Screen name="Voucher" component={Voucher} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}