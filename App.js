import { View, Text } from 'react-native'
import React, { useEffect, useState } from 'react'
import MainNavigation from './navigation/MainNavigation'
import { SafeAreaView } from 'react-native-safe-area-context'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Slide from './screens/Slide'


export default function App() {
  const [listUser, setListUser] = useState('');
  useEffect(() => {
    AsyncStorage.getItem('users').then(users => {
      console.log(users);
      if (users) {
        setListUser(JSON.parse(users))
      } else {
        AsyncStorage.setItem('users', JSON.stringify([]))
      }
    }
    ).catch(error => console.error(error));
  }, [])

  console.log(listUser);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>

      {/* <Slide /> */}

      <MainNavigation />

    </SafeAreaView>
  )
}

  // adb reverse tcp:8081 tcp:8081
