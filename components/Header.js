import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import Back from '../assets/svg/chevron-left.svg'


export default function Header({ onPress, title, children }) {
    return (
        <View style={{ flexDirection: 'row', position: 'relative', paddingVertical: 15 }}>
            {/* <Back style={{ marginLeft: 25, marginTop: 20 }} /> */}
            <TouchableOpacity onPress={onPress} style={{ position: 'absolute', left: 25, top: 20, zIndex: 999 }}>
                <Back />
            </TouchableOpacity>
            <Text style={{ flex: 1, textAlign: 'center', fontSize: 18, lineHeight: 27, fontWeight: '700', color: '#000' }}>{title}</Text>
        </View>

    )
}