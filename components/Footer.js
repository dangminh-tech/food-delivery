import { View, Text, Image, Dimensions } from 'react-native'
import React from 'react'
import Burger from '../assets/svg/hamburger.svg'
import Facebook from '../assets/svg/facebook 1.svg'
import Google from '../assets/svg/google-plus 1.svg'

const imgFooter = require('../assets/img/footer.png')

const appWidth = Dimensions.get('window').width


export default function Footer() {
    return (
        <View >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ width: 250, borderWidth: 0.5 }}></View>
                <Text style={{ marginLeft: 12, marginRight: 30 }}>Or connect with</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Image source={imgFooter} />
                <View style={{ flexDirection: 'row', marginRight: 30, width: 94 }}>
                    <Facebook style={{ marginRight: 10 }} />
                    <Google />
                </View>
            </View>

        </View>
    )
}