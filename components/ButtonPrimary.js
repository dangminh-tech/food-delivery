import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import React from 'react'

const appWidth = Dimensions.get('window').width;

export default function ButtonPrimary({ text, btnStyle, txtStyle, onPress }) {
    return (
        <TouchableOpacity style={[styles.base, { ...btnStyle }]} onPress={onPress} >
            <Text style={[styles.text, { ...txtStyle }]}>{text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    base: {
        width: appWidth - 60,
        height: 50,
        backgroundColor: '#D35400',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 18,
        fontWeight: '700',
        color: '#fff'
    }
})