import { View, Text, TextInput, StyleSheet, Dimensions } from 'react-native'
import React from 'react'
const appWidth = Dimensions.get('window').width


export default function InputPrimary({ placeholder, style, secure, value, setValue, onFocus, showKey }) {
    return (
        <TextInput
            placeholder={placeholder}
            style={[styles.input, { ...style }]}
            onFocus={onFocus}
            secureTextEntry={secure}
            onChangeText={(text) => setValue(text)}
            value={value}
            showSoftInputOnFocus={showKey}
        />
    )
}

const styles = StyleSheet.create({
    input: {
        width: appWidth - 60,
        height: 50,
        alignItems: 'center',
        paddingLeft: 30,
        backgroundColor: '#ECF0F1',
        borderRadius: 30
    }
})