import { View, Text, Image } from 'react-native'
import React from 'react'
import Back from '../assets/svg/chevron-left.svg'
import Loupe from '../assets/svg/loupe.svg'
import Pin from '../assets/svg/pin.svg'
import Pin2 from '../assets/svg/pin2.svg'
import Star from '../assets/svg/star.svg'
import Clock from '../assets/svg/clock.svg'


export default function Restaurant({ item, index }) {
  return (
    <View style={{ flexDirection: 'row', marginBottom: 15 }}>
      <Image source={require('../assets/img/restaurant.png')} />
      <View style={{ marginLeft: 20 }}>
        <Text style={{ fontSize: 15, fontWeight: '700' }}>{item.name}</Text>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
          <Pin2 />
          <Text> {item.address}</Text>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
          <Clock />
          <Text> {item.open}</Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 5 }}>
          {
            Array.from(Array(item.star).keys()).map((item, i) => {
              return <Star key={i} style={{ marginRight: 5 }} />
            })
          }
        </View>
      </View>
    </View>
  )
}
